
your_name = input("Hi, what's your name? ")
birth_month = int(input("What month were you born? "))
birth_year = int(input("What year were you born? "))

##################################

from random import randint


for guesses in range(1,6):
    guess_month = randint(1,12)
    guess_year = randint(1924,2004)

    print("Guess",guesses,": were you born in",guess_month,"/",guess_year,"?")

    response = input("Yes or No? ")

    if response == "Yes":
        print("I knew it!")
        exit()
    elif guesses == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
